class version():

    def __init__(self, major, minor=None, revision=None):
      self.major = major
      self.minor = minor
      self.revision = revision

    def __str__(self):
        if self.minor != None:
            if self.revision != None:
                return "{}.{}.{}".format(self.major, self.minor, self.revision)
            else:
                return "{}.{}".format(self.major, self.minor)
        else:
            return "{}".format(self.major)

def compareVer(v1, v2):
    select = None
    if v1.major < v2.major:
      select = v1
    elif v1.major > v2.major:
      select = v2
    else:
        if (v1.minor != None) and (v2.minor != None):
            if v1.minor < v2.minor:
                select = v1
            elif v1.minor > v2.minor:
                select = v2
            else:
                if (v1.revision != None) and (v2.revision):
                    if v1.revision < v2.revision:
                        select = v1
                    elif v1.revision > v2.revision:
                        select = v2
                    else:
                        select = v1
                elif (v1.revision == None) and (v2.revision != None):
                    select = v1
                elif (v1.revision != None) and (v2.revision == None):
                    select = v2
                else:
                    select = v1
        elif (v1.minor == None) and (v2.minor != None):
            select = v1
        elif (v1.minor != None) and (v2.minor == None):
            select = v2
        else:
            select = v1
    return select

def solution(l):
    result = []
    sortBuf = []
    temp = None
    cutPoint = 0
    for ver in l:
        temp = ver.split('.')
        if len(temp) == 3:
            sortBuf.append(version(int(temp[0]), int(temp[1]), int(temp[2])))
        elif len(temp) == 2:
            sortBuf.append(version(int(temp[0]), int(temp[1])))
        else:
            sortBuf.append(version(int(temp[0])))
    while len(sortBuf) != 0:
        temp = sortBuf[0]
        for ver in sortBuf:
            temp = compareVer(ver, temp)
        result.append(temp.__str__())
        sortBuf.remove(temp)
    return result

if __name__ == "__main__":
    testcases = [
        ["1.11", "2.0.0", "1.2", "2", "0.1", "1.2.1", "1.1.1", "2.0"],
        ["1.1.2", "1.0", "1.3.3", "1.0.12", "1.0.2"]
    ]
    for test in testcases:
        solution(test)
    