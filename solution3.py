from itertools import permutations, combinations

def isDiv3(num):
    if num%3 == 0:
        return True
    else:
        return False

def numeriser(num_list):
    size = len(num_list)
    result = 0
    for i in range(0,size):
        result += num_list[i]*(10**i)
    return result 

def solution(l):
    result = 0
    genBuf = []
    p = len(l)
    while p != 0:
        for cmb in combinations(l, p):
            genBuf += [numeriser(num) for num in permutations(cmb)]
        p -= 1
    genBuf.sort(reverse=True)
    for number in genBuf:
        if isDiv3(number):
            result = number
            break
    return result

if __name__ == "__main__":
    testcases = [[3, 1, 4, 1],[3, 1, 4, 1, 5, 9]]
    for testcase in testcases:
        print(solution(testcase))