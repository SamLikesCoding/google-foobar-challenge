def replacer(s):
  result = str()
  for x in s:
     if ord(x) in range(97, 123):
      result += chr(ord(x) + range(25, -26, -2)[ord(x) - 97])
     else:
      result += x
  return result

# checking purpose
if __name__ == "__main__":
  tests = [
         "Yvzs! I xzm'g yvorvev Lzmxv olhg srh qly zg gsv xlolmb!!", 
         "wrw blf hvv ozhg mrtsg'h vkrhlwv?"
  ]
  for case in tests:
       print(replacer(case))