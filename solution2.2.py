
def compareVer(v1, v2):
    param = 0
    select = None
    print("\n\nretrace : {} - {}".format(v1, v2))
    if v1 == v2:
      print("breakpoint -<EQUAL>-")
      select =  v1
    else:
        while (len(v1) >= param ) and (len(v2) >= param):
            if int(v1[param]) > int(v2[param]):
              select =  v2
              print("breakpoint -<GREATER>-")
              print("{} > {}".format(v1[param], v2[param]))
              break
            elif int(v1[param]) < int(v2[param]):
              select =  v1
              print("breakpoint -<LESS>-")
              print("{} < {}".format(v1[param], v2[param]))
              break
            else:
              if len(v1) < len(v2):
                print("breakpoint -<LEN_LESS>-")
                select =  v1
                break
              elif len(v2) < len(v1):
                print("breakpoint -<LEN_GREATER>-")
                select =  v2
                break
              else:
                 print("breakpoint -<NEXT_PARAM>-")
                 param += 1
    print("Select : {}".format(select))
    return select

def verSort(l):

    result = []
    sortBuf = []
    temp = None
    cutPoint = 0

    for item in l:
        sortBuf.append(item.split("."))
    while len(sortBuf) != 0:
        temp = sortBuf[0]
        for ver in sortBuf:
            temp = compareVer(ver, temp)
        result.append(temp)
        sortBuf.remove(temp)
    temp = "."
    cutPoint = len(result)
    for idx in range(0, cutPoint):
        result.append(temp.join(result[idx]))
    result = result[cutPoint:]
    print("Result : {}".format(result))
    #return result

if __name__ == "__main__":
    tests = [
        ["1.11", "2.0.0", "1.2", "2", "0.1", "1.2.1", "1.1.1", "2.0"],
        ["1.1.2", "1.0", "1.3.3", "1.0.12", "1.0.2"]
    ]
    verSort(tests[0])