def solution(x, y):
    M = int(x)
    F = int(y)
    result = 0
    temp = 0
    while True:
        if (M==1) and (F==1):
            break
        else:
            if (M <= 0) or (F <= 0):
                return "impossible"
            elif F == 1:
                return str(result + M - F)
            else:
                result += M//F
                temp = F
                F = M%F
                M = temp
    return str(int(result))

if __name__ == "__main__":
    testcases = [('2','1'), ('4','7')]
    for testcase in testcases:
        print(solution(testcase[0], testcase[1]))